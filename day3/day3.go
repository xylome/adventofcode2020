package main

import (
	"bufio"
	"fmt"
	"os"
)

var theMap [][]byte
var lineLength int
var lineNb int

type position struct {
	x, y int
}

var moves = []position{
	{1, 1},
	{3, 1},
	{5, 1},
	{7, 1},
	{1, 2},
}

func main() {
	lineNb = 0
	lineLength = 0
	nbTrees := 1
	theMap = make([][]byte, 1024)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		current := scanner.Text()
		if lineNb == 0 {
			lineLength = len(current)
		}
		theMap[lineNb] = parseLine(current)
		lineNb++
	}

	for _, move := range moves {
		nbTrees = walk(move) * nbTrees
	}

	fmt.Println("⭐️ found", nbTrees, "trees.")
}

func walk(move position) int {
	nbTrees := 0
	currentPos := position{0, 0} // not testing 0, 0
	for currentPos.y < lineNb-1 {
		currentPos = getNextPos(currentPos, move)
		if theMap[currentPos.y][currentPos.x] == byte('#') {
			nbTrees++
		}
	}
	return nbTrees
}

func getNextPos(current position, move position) position {
	current.x = current.x + move.x
	current.y = current.y + move.y

	if current.x >= lineLength {
		current.x = current.x - lineLength
	}

	return current
}

func parseLine(line string) []byte {
	result := make([]byte, lineLength)
	for i := 0; i < len(line); i++ {
		result[i] = line[i]
	}
	return result
}
