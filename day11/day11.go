package main

import (
	"bufio"
	"fmt"
	"os"
)

var theMap [][]string = make([][]string, 0)

type pos struct {
	X, Y int
}

var directions = []string{"N", "S", "E", "O", "NO", "NE", "SO", "SE"}

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	for scanner.Scan() {
		//current, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		var line []string = make([]string, 0)
		for _, current := range scanner.Text() {
			line = append(line, string(current))
		}
		theMap = append(theMap, line)
	}
	print(theMap)
	oldMap := fullCopy(theMap)

	/**** Part One
	for oldMap[0][0] = "a"; !equals(theMap, oldMap); {
		oldMap = fullCopy(theMap)
		theMap = makeARound(theMap)
	}
	fmt.Println("----")
	print(theMap)
	fmt.Println(countOccupied(theMap))
	**/

	/**** Part two ****/
	for oldMap[0][0] = "a"; !equals(theMap, oldMap); {
		oldMap = fullCopy(theMap)
		theMap = makeARound2(theMap)
	}
	fmt.Println("----")
	print(theMap)
	fmt.Println(countOccupied(theMap))

}

func countOccupied(tab [][]string) int {
	count := 0
	for _, lineTab := range tab {
		for _, value := range lineTab {
			if value == "#" {
				count++
			}
		}
	}

	return count
}

func noOneAround(poss []pos) bool {
	for _, pos := range poss {
		fmt.Println("no one", pos)
		if theMap[pos.X][pos.Y] == "#" {
			return false
		}
	}
	return true
}

func equals(a [][]string, b [][]string) bool {
	for line, lineVal := range a {
		for col, value := range lineVal {
			if b[line][col] != value {
				return false
			}
		}
	}
	return true
}

func tooMuchPeople(poss []pos) bool {
	count := 0
	for _, pos := range poss {
		fmt.Println("too much people at Y", pos.Y, "X", pos.X)
		if theMap[pos.X][pos.Y] == "#" {
			count++
		}
	}
	return count >= 4
}

func makeARound2(tab [][]string) [][]string {
	var orig = make([][]string, len(tab))
	var dest = make([][]string, len(tab))
	orig = fullCopy(tab)
	dest = fullCopy(tab)
	for currentLine, line := range orig {
		for currentCol, value := range line {
			switch value {
			case "L":
				count := countOccupiedInBeam(currentLine, currentCol)
				if count == 0 {
					fmt.Println("no one, sitting at", currentLine, currentCol)
					dest[currentLine][currentCol] = "#"
				} else {
					fmt.Println("too much people here,", count, " not sitting", currentLine, currentCol)
				}

			case "#":
				count := countOccupiedInBeam(currentLine, currentCol)
				fmt.Println("found", count, "at", currentLine, currentCol)
				if count >= 5 {
					fmt.Println("too much pople around, leaving", currentLine, currentCol)
					dest[currentLine][currentCol] = "L"
				} else {
					fmt.Println("I’m ok at", currentLine, currentCol)
				}

			}
		}
	}
	return dest
}

func noOneAroundInBeam(line int, col int) bool {
	//fmt.Println("no one in Beam for", line, col)
	for _, direction := range directions {
		currentDirectionBeamPos := getBeamPos(direction, line, col)
		for _, currentPos := range currentDirectionBeamPos {
			if theMap[currentPos.X][currentPos.Y] == "." {
				continue
			}
			if theMap[currentPos.X][currentPos.Y] == "#" {
				return false
			}
			if theMap[currentPos.X][currentPos.Y] == "L" {
				return true
			}
		}
	}
	return true
}

func countOccupiedInBeam(line int, col int) int {
	count := 0
	for _, direction := range directions {
		currentDirectionBeamPos := getBeamPos(direction, line, col)
		for _, currentPos := range currentDirectionBeamPos {
			if theMap[currentPos.X][currentPos.Y] == "." {
				continue
			}
			if theMap[currentPos.X][currentPos.Y] == "L" {
				break
			}
			if theMap[currentPos.X][currentPos.Y] == "#" {
				count++
				break
			}
		}
	}
	return count
}

func getBeamPos(direction string, line, col int) []pos {
	result := make([]pos, 0)
	switch direction {
	case "N":
		for currentX := line - 1; currentX >= 0; currentX-- {
			newPos := pos{X: currentX, Y: col}
			result = append(result, newPos)
		}
	case "S":
		for currentX := line + 1; currentX < len(theMap); currentX++ {
			newPos := pos{X: currentX, Y: col}
			result = append(result, newPos)
		}
	case "O":
		for currentY := col - 1; currentY >= 0; currentY-- {
			newPos := pos{X: line, Y: currentY}
			result = append(result, newPos)
		}
	case "E":
		for currentY := col + 1; currentY < len(theMap[0]); currentY++ {
			newPos := pos{X: line, Y: currentY}
			result = append(result, newPos)
		}
	case "NO":
		currentX := line - 1
		currentY := col - 1
		for currentX >= 0 && currentY >= 0 {
			newPos := pos{X: currentX, Y: currentY}
			result = append(result, newPos)
			currentX--
			currentY--
		}
	case "NE":
		currentX := line - 1
		currentY := col + 1
		for currentX >= 0 && currentY < len(theMap[0]) {
			newPos := pos{X: currentX, Y: currentY}
			result = append(result, newPos)
			currentX--
			currentY++
		}
	case "SO":
		currentX := line + 1
		currentY := col - 1
		for currentX < len(theMap) && currentY >= 0 {
			newPos := pos{X: currentX, Y: currentY}
			result = append(result, newPos)
			currentX++
			currentY--
		}
	case "SE":
		currentX := line + 1
		currentY := col + 1
		for currentX < len(theMap) && currentY < len(theMap[0]) {
			newPos := pos{X: currentX, Y: currentY}
			result = append(result, newPos)
			currentX++
			currentY++
		}
	}

	return result
}

func makeARound(tab [][]string) [][]string {
	var orig = make([][]string, len(tab))
	var dest = make([][]string, len(tab))
	orig = fullCopy(tab)
	dest = fullCopy(tab)
	for currentLine, line := range orig {
		for currentCol, value := range line {
			adjacents := getAdacentPos(pos{X: currentLine, Y: currentCol}, orig)
			switch value {
			case "L":
				if noOneAround(adjacents) {
					dest[currentLine][currentCol] = "#"
				}
			case "#":
				if tooMuchPeople(adjacents) {
					dest[currentLine][currentCol] = "L"
				}
			}
		}
	}
	return dest
}

func print(tab [][]string) {
	for id, line := range tab {
		fmt.Println(id, line, len(line)-1)
	}
}

func fullCopy(src [][]string) [][]string {
	dest := make([][]string, 0)
	for _, val := range src {
		tmp := make([]string, 0)
		for _, value := range val {
			tmp = append(tmp, value)
		}
		dest = append(dest, tmp)
	}
	return dest
}

func getAdacentPos(a pos, in [][]string) []pos {
	fmt.Println("adjacents for X", a.X, "Y", a.Y)
	var result = make([]pos, 0)
	originX := a.X - 1
	if originX < 0 {
		originX = 0
	}
	originY := a.Y - 1
	if originY < 0 {
		originY = 0
	}
	maxX := a.X + 1
	if maxX >= len(in) {
		maxX = len(in) - 1
	}

	maxY := a.Y + 1
	if maxY >= len(in[0]) {
		maxY = len(in[0]) - 1
	}

	for i := originX; i <= maxX; i++ {
		for j := originY; j <= maxY; j++ {
			if i == a.X && j == a.Y {
				continue
			}
			foo := pos{X: i, Y: j}
			result = append(result, foo)
		}
	}
	fmt.Println(result)
	return result
}
