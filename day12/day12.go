package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"

	"gonum.org/v1/gonum/mat"
)

type pos struct {
	x, y int
}
type instruction struct {
	action string
	value  int
}

var counterClockwise = []float64{0, -1, 1, 0}
var clockWise = []float64{0, 1, -1, 0}
var tab180 = []float64{-1, 0, 0, -1}
var counterClockwiseMat = mat.NewDense(2, 2, counterClockwise)
var clockWiseMat = mat.NewDense(2, 2, clockWise)
var mat180 = mat.NewDense(2, 2, tab180)
var heading = 90
var currentPos = pos{x: 0, y: 0}
var waypointPos = pos{x: 10, y: 1}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		currentInstruction := parseInstruction(scanner.Text())
		fmt.Println(currentInstruction)
		processInstruction2(currentInstruction)
		fmt.Println("ship is now:", currentPos, "waypoint is now:", waypointPos)
	}

	fmt.Println("⭐️⭐️  ", manahattanDistance(currentPos))

}

func pos2mtatrix(p pos) *mat.Dense {
	tab := []float64{float64(p.x), float64(p.y)}
	return mat.NewDense(2, 1, tab)
}

func parseInstruction(s string) instruction {
	result := instruction{action: string(s[0])}
	value, _ := strconv.Atoi(s[1:])
	result.value = value
	return result
}

func manahattanDistance(p pos) int {
	return int(math.Abs(float64(p.x))) + int(math.Abs(float64(p.y)))
}

func processInstruction2(i instruction) {
	switch i.action {
	case "L":
		processWaypointRotation(i)
	case "R":
		processWaypointRotation(i)
	case "E":
		moveWaypoint(i)
	case "W":
		moveWaypoint(i)
	case "N":
		moveWaypoint(i)
	case "S":
		moveWaypoint(i)
	case "F":
		moveShip(i)
	}

}

func moveShip(i instruction) {
	currentPos.x += waypointPos.x * i.value
	currentPos.y += waypointPos.y * i.value
}

func moveWaypoint(i instruction) {
	switch i.action {
	case "N":
		waypointPos.y += i.value
	case "E":
		waypointPos.x += i.value
	case "S":
		waypointPos.y -= i.value
	case "W":
		waypointPos.x -= i.value
	}
}

func processWaypointRotation(i instruction) {
	var tmp mat.Dense
	src := pos2mtatrix(waypointPos)

	switch i.value {
	case 180:
		tmp.Mul(mat180, src)
	case 270:
		if i.action == "R" {
			tmp.Mul(counterClockwiseMat, src)
		} else {
			tmp.Mul(clockWiseMat, src)
		}
	case 90:
		if i.action == "L" {
			tmp.Mul(counterClockwiseMat, src)
		} else {
			tmp.Mul(clockWiseMat, src)
		}
	}
	waypointPos.x = int(tmp.At(0, 0))
	waypointPos.y = int(tmp.At(1, 0))
}

func processInstruction1(i instruction) {
	switch i.action {
	case "L":
		proceesHeading(i)
	case "R":
		proceesHeading(i)
	case "E":
		move(i)
	case "W":
		move(i)
	case "N":
		move(i)
	case "S":
		move(i)
	case "F":
		switch heading {
		case 0:
			i.action = "N"
		case 90:
			i.action = "E"
		case 180:
			i.action = "S"
		case 270:
			i.action = "W"
		}
		move(i)
	}

}

func move(i instruction) {
	switch i.action {
	case "N":
		currentPos.y += i.value
	case "E":
		currentPos.x += i.value
	case "S":
		currentPos.y -= i.value
	case "W":
		currentPos.x -= i.value
	}
}

func proceesHeading(i instruction) {
	if i.action == "L" {
		heading -= i.value
		if heading < 0 {
			heading = 360 - int(math.Abs(float64(heading)))
		}
	}
	if i.action == "R" {
		heading += i.value
		if heading >= 360 {
			heading = heading - 360
		}
	}

}
