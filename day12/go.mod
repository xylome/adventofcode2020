module day12

go 1.13

require (
	golang.org/x/exp v0.0.0-20201210212021-a20c86df00b4 // indirect
	golang.org/x/tools v0.0.0-20201211185031-d93e913c1a58 // indirect
	gonum.org/v1/gonum v0.8.2
)
