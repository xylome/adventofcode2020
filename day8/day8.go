package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

type instruction struct {
	command   string
	value     int
	processed bool
}

var acc = 0

func main() {
	var bootCode = []instruction{}

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		bootCode = append(bootCode, parse(scanner.Text()))
	}
	var originalCode = make([]instruction, len(bootCode))
	copy(originalCode, bootCode)

	run(bootCode)
	fmt.Println("⭐️  ", acc)

	acc = 0
	currentCode := make([]instruction, len(bootCode))
	copy(bootCode, originalCode)
	for i := 0; i < len(bootCode); i++ {
		copy(currentCode, bootCode)
		currentCode = patch(currentCode, i)
		acc = 0
		ok := run(currentCode)
		if ok {
			break
		}
	}

	fmt.Println("⭐️⭐️", acc)
}

func patch(c []instruction, i int) []instruction {
	dup := make([]instruction, len(c))
	copy(dup, c)
	switch dup[i].command {
	case "jmp":
		dup[i].command = "nop"
	case "nop":
		dup[i].command = "jmp"
	}
	return dup
}

func run(code []instruction) bool {
	currentIdx := 0
	for {
		if currentIdx >= len(code) {
			return true
		}
		if code[currentIdx].processed {
			return false
		}
		code[currentIdx].processed = true
		switch code[currentIdx].command {
		case "acc":
			acc += code[currentIdx].value
			currentIdx++
		case "jmp":
			currentIdx = currentIdx + code[currentIdx].value
		case "nop":
			currentIdx++
		}
	}
}

func parse(s string) instruction {
	var result = instruction{processed: false}
	splitted := strings.Split(s, " ")
	result.command = splitted[0]
	val, _ := strconv.Atoi(splitted[1])
	result.value = val
	return result
}
