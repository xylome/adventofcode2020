package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	currentGroupSring := ""
	part1 := 0
	part2 := 0
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		currentLine := scanner.Text()
		if currentLine != "" {
			currentGroupSring = currentGroupSring + currentLine + "\n"
		} else {
			groupResPart1, groupResPart2 := parseGroup(currentGroupSring)
			currentGroupSring = ""
			part1 += groupResPart1
			part2 += groupResPart2
		}

	}
	fmt.Println("⭐️   total", part1)
	fmt.Println("⭐️⭐️ total", part2)
}

func parseGroup(s string) (int, int) {
	s = strings.Trim(s, "\n")
	groupAnswers := make(map[string]int)
	splitted := strings.Split(s, "\n")
	for _, person := range splitted {
		for _, answer := range person {
			_, ok := groupAnswers[string(answer)]
			if !ok {
				groupAnswers[string(answer)] = 1
			} else {
				groupAnswers[string(answer)]++
			}
		}
	}

	part2 := 0
	for _, val := range groupAnswers {
		if val == len(splitted) {
			part2++
		}
	}

	return len(groupAnswers), part2
}
