package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

var re *regexp.Regexp

func main() {
	re = regexp.MustCompile("^(\\d+)-(\\d+)\\s(\\w):\\s(.*)?")
	validPasswords := 0
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		current := scanner.Text()
		if isValidPasswordStep2(parse(current)) {
			validPasswords++
		}
	}

	fmt.Println("⭐️", validPasswords, "valid passwords")
}

func isValidPassword(min, max int64, needle string, password string) bool {
	count := timesAppearIn(needle, password)
	if count <= max && count >= min {
		return true
	}
	return false
}

func isValidPasswordStep2(min, max int64, needle string, password string) bool {
	condition1 := string(password[min-1]) == needle
	condition2 := string(password[max-1]) == needle

	return condition1 != condition2
}

func parse(line string) (int64, int64, string, string) {
	result := re.FindStringSubmatch(line)
	min, _ := strconv.ParseInt(result[1], 10, 64)
	max, _ := strconv.ParseInt(result[2], 10, 64)
	return min, max, result[3], result[4]
}

func timesAppearIn(needle, password string) int64 {
	count := 0
	for i := 0; i < len(password); i++ {
		if string(password[i]) == needle {
			count++
		}
	}
	return int64(count)
}
