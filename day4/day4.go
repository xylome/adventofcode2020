package main

import (
	"bufio"
	"fmt"
	"os"
	"regexp"
	"strconv"
)

type passport struct {
	byr,
	iyr,
	eyr,
	hgt,
	hcl,
	ecl,
	pid,
	cid string
}

var reByr, reIyr, reEyr, reHgt, reHcl, reEcl, rePid, reCid *regexp.Regexp

func main() {
	passportString := ""
	validPassportNb := 0
	passwordCount := 0
	scanner := bufio.NewScanner(os.Stdin)
	initRegexp()
	for scanner.Scan() {
		current := scanner.Text()
		if current != "" {
			passportString = passportString + " " + current
		} else {
			passwordCount++
			currentPassport := parsePassport(passportString)
			//fmt.Println("Current passport", passportString)

			if isValid(currentPassport) {
				fmt.Println(passwordCount-1, passportString, "is valid passport")
				validPassportNb++
			}
			//fmt.Println("-- v:", validPassportNb, "on", passwordCount)
			//fmt.Println(" ")
			passportString = ""
		}
	}
	fmt.Println("⭐️", validPassportNb, "valid passwords")
}

func isValid(p passport) bool {
	if p.eyr == "" {
		fmt.Println("eyr is missing")
		return false
	} else {
		if !isValidYear(p.eyr, 2020, 2030) {
			fmt.Println("eyr is not valid")
			return false
		} else {
			fmt.Println("eyr is valid")
		}
	}

	if p.hcl == "" {
		fmt.Println("hcl is missing or invalid")
		return false
	} else {
		fmt.Println("hcl is valid")
	}

	if p.pid == "" {
		fmt.Println("pid is missing or invalid")
		return false
	} else {
		if len(p.pid) != 9 {
			fmt.Println("pid is invalid length")
			return false
		}
		fmt.Println("pid is valid")
	}

	if p.ecl == "" {
		fmt.Println("ecl is missing or invalid")
		return false
	} else {
		fmt.Println("ecl is valid")
	}

	if p.hgt == "" {
		fmt.Println("hgt is missing")
		return false
	} else {
		if !isValidHgt(p.hgt) {
			fmt.Println("hgt not valid")
			return false
		} else {
			fmt.Println("hgt is valid")
		}
	}

	if p.iyr == "" {
		fmt.Println("iyr is missing")
		return false
	} else {
		if !isValidYear(p.iyr, 2010, 2020) {
			fmt.Println("iyr is not valid")
			return false
		} else {
			fmt.Println("iyr is valid")
		}
	}

	if p.byr == "" {
		fmt.Println("byr is missing")
		return false
	} else {
		if !isValidYear(p.byr, 1920, 2002) {
			fmt.Println("byr is not valid")
			return false
		} else {
			fmt.Println("byr is valid")
		}
	}

	if p.cid == "" {
		fmt.Println("cid is missing, but it’s fine.")
		return true
	}
	return true
}

func isValidHgt(hgt string) bool {
	re := regexp.MustCompile("(\\d+)(in|cm)")
	result := re.FindStringSubmatch(hgt)
	if len(result) == 0 {
		fmt.Println("bad format for hgt")
		return false
	}

	b, err := strconv.ParseInt(result[1], 10, 64)
	if err != nil {
		fmt.Println("invalid int")
		return false
	}

	if result[2] == "cm" {
		return b >= 150 && b <= 193
	} else {
		return b >= 59 && b <= 76
	}
}

func isValidYear(year string, min, max int) bool {
	b, err := strconv.ParseInt(year, 10, 64)
	if err != nil {
		fmt.Println("invalid int")
		return false
	}

	return b >= int64(min) && b <= int64(max)
}

func initRegexp() {
	reByr = regexp.MustCompile("byr:(\\w+)\\s?")
	reIyr = regexp.MustCompile("iyr:(\\w+)\\s?")
	reEyr = regexp.MustCompile("eyr:(\\w+)\\s?")
	reHgt = regexp.MustCompile("hgt:(\\w+)\\s?")
	reHcl = regexp.MustCompile("hcl:(#[0-9a-f]{6})\\s?")
	reEcl = regexp.MustCompile("ecl:(amb|blu|brn|gry|grn|hzl|oth)\\s?")
	rePid = regexp.MustCompile("pid:(\\d+)")
	reCid = regexp.MustCompile("cid:(#?\\w+)\\s?")
}

func parsePassport(passportString string) passport {
	result := passport{}

	regexResult := reByr.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.byr = regexResult[1]
	}
	regexResult = reIyr.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.iyr = regexResult[1]
	}
	regexResult = reEyr.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.eyr = regexResult[1]
	}
	regexResult = reHgt.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.hgt = regexResult[1]
	}
	regexResult = reHcl.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.hcl = regexResult[1]
	}
	regexResult = reEcl.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.ecl = regexResult[1]
	}
	regexResult = rePid.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.pid = regexResult[1]
	}
	regexResult = reCid.FindStringSubmatch(passportString)
	if len(regexResult) > 0 {
		result.cid = regexResult[1]
	}

	return result
}
