package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var xmas []int
var preambule = 25

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		current, _ := strconv.Atoi(scanner.Text())
		xmas = append(xmas, current)

	}
	step1 := run()
	fmt.Println("⭐️  ", step1)
	minID, maxID := findSegmentsIdxSum(step1)
	step2 := xmas[minID : maxID+1]
	min, max := minMax(step2)
	fmt.Println("⭐️⭐️", min+max)
}

func minMax(tab []int) (int, int) {
	max := 0
	min := 999999999999
	for _, val := range tab {
		if val > max {
			max = val
		}
		if val < min {
			min = val
		}
	}
	return min, max
}

func run() int {
	for i := preambule - 1; i < len(xmas)-1; i++ {
		if !find(xmas[i+1], xmas[i-preambule+1:i+1]) {
			return xmas[i+1]
		}
	}
	return 0
}

func findSegmentsIdxSum(needle int) (int, int) {
	for idxa, min := range xmas[:len(xmas)-1] {
		total := min
		for idxb, max := range xmas[idxa+1:] {
			total += max
			if total == needle {
				return idxa, idxb + idxa + 1
			}
			if total > needle {
				break
			}
		}
	}
	return 0, 0
}

func find(needle int, sxmas []int) bool {
	for idx, a := range sxmas[:len(sxmas)-1] {
		for _, b := range sxmas[idx+1:] {
			if a+b == needle {
				return true
			}
		}
	}
	return false
}
