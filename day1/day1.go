package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

var expenses []int64 = make([]int64, 0)

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		current, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		expenses = append(expenses, current)
	}
	find2020()
}

func find2020() {
	for idx1, val1 := range expenses {
		for idx2, val2 := range expenses {
			for idx3, val3 := range expenses {
				if (idx1 != idx2) && (idx3 != idx2) && (idx3 != idx1) {
					if val1+val2+val3 == 2020 {
						fmt.Println("⭐️", val1, val2, val3, val1*val2*val3)
						return
					}
				}
			}
		}
	}
	fmt.Println("no answers 🤷")
}
