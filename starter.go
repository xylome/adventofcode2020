package main

import "fmt"
import "os"
import "bufio"

import "strconv"



func main() {
	var total int64

	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		current, _ := strconv.ParseInt(scanner.Text(), 10, 64)
		total += current
	}
	fmt.Println("Total:", total)
}
