package main

import (
	"bufio"
	"fmt"
	"log"
	"math"
	"os"
	"sort"
)

type rowColSpec struct {
	min, max int
}

func main() {
	seats := make([]int, 0)
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		currentLine := scanner.Text()
		seats = append(seats, getSeatID(currentLine))

	}
	sort.Ints(seats)
	fmt.Println("⭐️ Max seat ID", seats[len(seats)-1])
	fmt.Println("⭐️⭐️ My seat ID", getMySeat(seats))
}

func getMySeat(seats []int) int {
	for idx, value := range seats[0 : len(seats)-1] {
		if value == seats[idx+1]-1 {
			continue
		} else {
			return value + 1
		}
	}
	log.Fatal("Cannot find my seat")
	return 0
}

func getSeatID(s string) int {
	row := getRow(s[0:7])
	col := getCol(s[7:10])
	result := row*8 + col
	return result
}

func getRow(s string) int {
	rowRes := rowColSpec{min: 0, max: 127}
	for _, sigle := range s {
		rowRes = resolveRowRange(rowRes, string(sigle))
	}
	return rowRes.min
}

func getCol(s string) int {
	colRes := rowColSpec{min: 0, max: 7}
	for _, sigle := range s {
		colRes = resolveRowRange(colRes, string(sigle))
	}
	return colRes.min
}

func resolveRowRange(r rowColSpec, s string) rowColSpec {
	switch s {
	case "F":
		r.max = r.max - int(math.Ceil(float64(r.max-r.min)/2))
	case "L":
		r.max = r.max - int(math.Ceil(float64(r.max-r.min)/2))
	case "B":
		r.min = r.min + int(math.Ceil(float64(r.max-r.min)/2))
	case "R":
		r.min = r.min + int(math.Ceil(float64(r.max-r.min)/2))
	default:
		log.Fatal("unkwown format")
	}
	return r
}
